# Path to your oh-my-zsh installation.
export ZSH=/home/hayden/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
ZSH_THEME="hayden"

# enable command auto-correction.
# ENABLE_CORRECTION="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(command-not-found sudo wd)

# User configuration

export PATH=$(yarn global bin):$PATH
export PATH=$HOME/.local/bin:$PATH

source $ZSH/oh-my-zsh.sh
source /usr/share/nvm/init-nvm.sh

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
alias indent="indent -kr -nut"
alias nemo="nemo --no-desktop"
alias zshrc="vim ~/.zshrc"
alias apti="sudo apt install"
alias aptr="sudo apt remove"
alias dog="pygmentize -g"
alias gst="git status"
alias gd="git diff"

# Make it so ls doesn't give horrible green backgrounds for
# directories inside of symlinks
export LS_COLORS="rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=01;34:st=37;44:ex=01;32:"

export DOTNET_CLI_TELEMETRY_OPTOUT=1
