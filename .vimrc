" VUNDLE
set nocompatible
filetype off

" set runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage itself
" type :PluginInstall after adding new plugins to this list
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin 'Valloric/YouCompleteMe'

call vundle#end()
filetype plugin indent on

" airline
let g:airline#extensions#tabline#enabled = 1 " enable list of buffers
let g:airline#extensions#tabline#fnamemod = ':t' " show just the filename
let g:airline_theme='powerlineish'

" colours
colorscheme material-theme
syntax enable

" set leader key to comma
let mapleader=","

" functionality
set confirm " prompt to save before quitting
set mouse=a " enable use of mouse to move cursor

" typing
set backspace=indent,eol,start " bksp works over linebreaks, indentation and past where insert mode started

" UI
set number " line numbers
set relativenumber " relative instead of absolute line numbers
set showcmd " show command in bottom right as you type it
set wildmenu " show a visual menu for tab completion
set cursorline " underline the line the cursor is currently on
set ruler " display cursor position at bottom of screen
set showmatch " highlight matching brackets when hovering over one
set nowrap " don't do line wrapping

" Enable wrap for tex files
augroup WrapLineInTexFile
    autocmd!
    autocmd FileType tex setlocal wrap
augroup END

" indenting
set shiftwidth=4 " number of spaces to add when using autoindent
set softtabstop=4 " number of spaces to add when typing a tab
set tabstop=4 " number of spaces to display when reading a tab
set expandtab " tabs are spaces
set autoindent " self explanitory

" searching
set ignorecase " case insensitive searching
set smartcase " search pattern containing uppercase is case sensitive
set hlsearch " highlight all matches when searching
set incsearch " search automatically as you type
" unhighlight
nnoremap <leader><space> :nohlsearch<CR>

" KEY MAPPINGS
" noremap = non recursive mapping, map = recursive mapping
" prepend n/v to only map for normal/visual modes

" cut, copy paste to OS register
map <leader>d "+d
map <leader>x "+x
map <leader>y "+y
nmap <leader>p "+p

" save
map <leader>w :w<CR>

" close
map <leader>q :q<CR>

" select all
nmap <leader>a ggVG

" highlight last inserted text
nmap gV `[v`]

" compile using make
nmap <leader>m :!make<CR>

" BUFFERS
nmap <leader>l :bl<CR>
nmap <leader>k :bn<CR>
nmap <leader>h :br<CR>
nmap <leader>j :bp<CR>
nmap <leader><CR> :bd<CR>

" compile/run depending on filetype
nmap <leader>n :!compile %<CR>

" Move up/down within wrapped lines
map <up> gk
map <down> gj

" Search for the word under the cursor
nnoremap // yw/<C-R>"<CR>
vnoremap // y/<C-R>"<CR>

" automatically add closing braces
inoremap { {}<Left>
inoremap {<CR> {<CR>}<Esc>ko
inoremap {} {}
inoremap <expr> }  strpart(getline('.'), col('.')-1, 1) == "}" ? "\<Right>" : "}"

inoremap ( ()<Esc>i
inoremap <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"

inoremap [ []<Esc>i
inoremap <expr> ]  strpart(getline('.'), col('.')-1, 1) == "]" ? "\<Right>" : "]"

inoremap <expr> " strpart(getline('.'), col('.')-1, 1) == "\"" ? "\<Right>" : "\"\"\<Left>"

inoremap <expr> <BS> index(["\"\"", "()", "[]", "{}"], strpart(getline('.'), col('.')-2, 2)) != -1 ? "\<Right><BS><BS>" : "\<BS>"

inoremap /** /**<CR>/<Esc>O

inoremap <?<CR> <?php<CR>?><Esc>O
inoremap <? <?php  ?><Esc>2hi
