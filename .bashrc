export PS1="\[\033[01;32m\]\u \[\033[01;34m\]\\w $ \[\033[01;00m\]"
alias indent="indent -kr -nut"
alias nemo="nemo --no-desktop"
