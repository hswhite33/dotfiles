#!/usr/bin/env python3

import json
from pathlib import Path
import sys

rawData = {}

try:
    with open(f"{Path.home()}/.config/Google Play Music Desktop Player/json_store/playback.json") as dataFile:
        rawData = json.loads(dataFile.read())
except:
    print("")
    sys.exit()

song = rawData["song"]
title = song["title"]
artist = song["artist"]
playing = rawData["playing"]

if title is None:
    print("")

def truncate(val):
    if len(val) < 20:
        return val
    return f"{val[:20]}..."

symbol = "" if playing else ""

print(f"{symbol} {truncate(artist)} - {truncate(title)}")
