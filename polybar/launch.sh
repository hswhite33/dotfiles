#!/bin/bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

bspc config top_padding 18

polybar bar0 &
polybar bar1 &
