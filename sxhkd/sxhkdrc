#
# bspwm hotkeys
#

#restart X
ctrl + alt + BackSpace
	bspc quit

#close active window
super + w
	bspc node -c

#toggle stacking/tiling
super + t
	bspc desktop -l next

#make everything the same size
super + b
	bspc node @focused:/ -B

#reset sizes
super + v
        bspc node @focused:/ -E

#toggle floating/fullscreen
super + {s,d,f}
	bspc node -t {floating,tiled,fullscreen}

#switch between last active desktop
super + Tab
	bspc desktop -f last

#swap current window with biggest
super + m
	bspc node -s biggest.local

#switch window in all directions
super + {Left,Down,Up,Right}
	bspc node -f {west,south,north,east}

alt + {h,j,k,l}
	bspc node -f {west,south,north,east}

#swap window in all directions
super + alt + {Left,Down,Up,Right}
	bspc node -s {west,south,north,east} --follow

super + alt + {h,j,k,l}
	bspc node -s {west,south,north,east} --follow

#preselect direction to open next window
super + ctrl + {Left,Down,Up,Right}
	bspc node -p {west,south,north,east}

#preselect full desktop
super + shift + ctrl + {Left,Down,Up,Right}
    bspc node @focused:/ -f; \
    bspc node -p {west,south,north,east}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#resize window
super + alt + {h,j,k,l}
	bspc node -e {left -10,down +10,up -10,right +10}

#resize window
super + alt + shift + {h,j,k,l}
	bspc node -e {right -10,up +10,down -10,left +10}

#set splitting ratio
super + ctrl + {1-9}
	bspc node -r 0.{1-9}

#switch to/move window to desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} {1-9,0}

#move window to desktop and follow it
super + shift + ctrl + {1-9,0}
    bspc node -d {1-9,0} --follow

#move window to monitor
super + shift + {Left,Right}
    bspc node -m {west,east} --follow

#focus window by clicking on it
~button{1-3}
	bspc pointer -g focus

#move/resize windows with mouse
super + button{1-3}
	; bspc pointer -g {move,resize_side,resize_corner}

#terminate pointer action (??)
super + @button{1-3}
	bspc pointer -u

#switch to/move window to first empty desktop
super + {_,shift + } grave
    bspc {desktop -f,node -d} next.\!occupied

#follow window to first empty desktop
super + shift + ctrl + grave
    bspc node -d $(bspc query -D -d next.\!occupied) --follow
   

#
# logitech mouse stuff
#

button10
    xte 'key F5'

#
# wm independent hotkeys
#

#terminal
super + {Return,KP_Enter} 
	urxvt

#launcher
alt + d
	rofi -show run

#locker
super + l
        /home/hayden/repos/dotfiles/.blurlocker

# https://github.com/boredomwontgetus/pa-vol
#volume down
XF86AudioLowerVolume
    pa-vol minus

#volume up
XF86AudioRaiseVolume
    pa-vol plus

#volume mute
XF86AudioMute
    pa-vol mute

#play/pause
{XF86AudioPlay,XF86AudioStop}
    playerctl {play-pause,stop}

#skip track
{XF86AudioPrev,XF86AudioNext}
    playerctl {previous,next}


#upload selection
ctrl + Print
    flameshot gui -p ~/Pictures/Screenshots

#screenshot selection
ctrl + shift + Print
    imgur -s -n

#upload desktop
ctrl + alt + 1
    imgur -f

#screenshot desktop
ctrl + alt + shift + 1
    imgur -f -n

#screenshot desktop to clipboard
Print
        gnome-screenshot -c

#upload window
ctrl + alt + 2
    imgur -w

#screenshot window
ctrl + alt + shift + 2
    imgur -w -n

#file browser/university folder
super + e
        nautilus

#open browser
super + alt + {Return, KP_Enter}
        firefox

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd
